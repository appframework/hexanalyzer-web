## [1.0.27](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.26...1.0.27) (2025-03-01)

## [1.0.26](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.25...1.0.26) (2025-02-26)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.28 ([d37622f](https://gitlab.com/appframework/hexanalyzer-web/commit/d37622f99d1d2a7d855b4cbfc6ce3b6ce60cbf04))
* **deps:** update dependency bootstrap-vue-next to v0.26.30 ([d5a4162](https://gitlab.com/appframework/hexanalyzer-web/commit/d5a416202b41fb3df8b439f40497b10c2f67fdfc))
* **deps:** update quarkus.platform.version to v3.18.3 ([84d94da](https://gitlab.com/appframework/hexanalyzer-web/commit/84d94da1d1ac8c9c698edd20d82bf531195e1417))

## [1.0.25](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.24...1.0.25) (2025-02-12)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.5.0 ([e1ee2ca](https://gitlab.com/appframework/hexanalyzer-web/commit/e1ee2ca95a6e5b51d52ad028c0e184f4c17a3f02))
* **deps:** update dependency bootstrap-vue-next to v0.26.21 ([d2b44e2](https://gitlab.com/appframework/hexanalyzer-web/commit/d2b44e2f55fe52ff6ae944e3b3f236440f05ccaf))
* **deps:** update dependency bootstrap-vue-next to v0.26.22 ([0216bb0](https://gitlab.com/appframework/hexanalyzer-web/commit/0216bb0b0e91617c9aff5b24a22ec059dda69465))
* **deps:** update dependency bootstrap-vue-next to v0.26.26 ([ca57516](https://gitlab.com/appframework/hexanalyzer-web/commit/ca575166675dd9b49fe5f27050e953ac59c24134))
* **deps:** update dependency primelocale to v1.5.0 ([9496d39](https://gitlab.com/appframework/hexanalyzer-web/commit/9496d397b51045398aa8b157ea6e0e56edee0637))
* **deps:** update dependency primelocale to v1.6.0 ([9b14d30](https://gitlab.com/appframework/hexanalyzer-web/commit/9b14d301a4e311177f4a9e6113b3f7eb0d66f70a))
* **deps:** update dependency vue-i18n to v11.1.0 ([ccb3529](https://gitlab.com/appframework/hexanalyzer-web/commit/ccb3529134d782ee61d95d11264cd8776607fb15))
* **deps:** update dependency vue-i18n to v11.1.1 ([cf31a9d](https://gitlab.com/appframework/hexanalyzer-web/commit/cf31a9d6b28fedc757d73f2d17ed18c52a739fa9))
* **deps:** update quarkus.platform.version to v3.18.0 ([177a2e8](https://gitlab.com/appframework/hexanalyzer-web/commit/177a2e8cb3def0a6913ec0208e0e13086d3b2f93))
* **deps:** update quarkus.platform.version to v3.18.1 ([a637ada](https://gitlab.com/appframework/hexanalyzer-web/commit/a637adaef0322a0365f8a8488052a1c61fd65689))
* **deps:** update quarkus.platform.version to v3.18.2 ([62f408a](https://gitlab.com/appframework/hexanalyzer-web/commit/62f408a18ea5e4a22a38bc7a732d4b2e9b8c4bc8))

## [1.0.24](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.23...1.0.24) (2025-01-22)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.20 ([d5a114f](https://gitlab.com/appframework/hexanalyzer-web/commit/d5a114fcd639f8ec6c000a0acfb003b8a94fcc6c))
* **deps:** update dependency primelocale to v1.4.0 ([5822fba](https://gitlab.com/appframework/hexanalyzer-web/commit/5822fba57052fe47632da9cffd6ba813dc414bc2))
* **deps:** update quarkus.platform.version to v3.17.7 ([13ae74e](https://gitlab.com/appframework/hexanalyzer-web/commit/13ae74ec0804ccf759753afe9de1f751fe2ccb45))

## [1.0.23](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.22...1.0.23) (2025-01-16)

## [1.0.22](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.21...1.0.22) (2025-01-15)


### Bug Fixes

* **deps:** update dependency primelocale to v1.3.0 ([1ab613e](https://gitlab.com/appframework/hexanalyzer-web/commit/1ab613eefea6cc512181c4b205e7aafa9bdb1de5))

## [1.0.21](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.20...1.0.21) (2025-01-15)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.4.0 ([f1396a0](https://gitlab.com/appframework/hexanalyzer-web/commit/f1396a07bdafc4f87a7a5b03ce0923e41dcd6d05))
* **deps:** update quarkus.platform.version to v3.17.6 ([789376b](https://gitlab.com/appframework/hexanalyzer-web/commit/789376b453cf175eef58c8d731e67d8f354f2e60))

## [1.0.20](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.19...1.0.20) (2025-01-09)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12.3.0 ([28b8373](https://gitlab.com/appframework/hexanalyzer-web/commit/28b8373130281659a713fbe75b8bd4818b82ae38))
* **deps:** update dependency vue-i18n to v11 ([f9bbb97](https://gitlab.com/appframework/hexanalyzer-web/commit/f9bbb97e26a8ed20e654821908b6fcf9c55e0d97))

## [1.0.19](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.18...1.0.19) (2025-01-08)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.19 ([fbd5af8](https://gitlab.com/appframework/hexanalyzer-web/commit/fbd5af879d6a329aa03913d2027ce8d3525737c8))
* **deps:** update dependency primelocale to v1.2.3 ([01531c5](https://gitlab.com/appframework/hexanalyzer-web/commit/01531c50cf8665e929cd9619475384ed660b03bb))

## [1.0.18](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.17...1.0.18) (2025-01-07)

## [1.0.17](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.16...1.0.17) (2025-01-01)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.18 ([fb01670](https://gitlab.com/appframework/hexanalyzer-web/commit/fb016702d6d5d0ad4c4dc0b63ad694a334f8a0d0))
* **deps:** update quarkus.platform.version to v3.17.5 ([2ad891f](https://gitlab.com/appframework/hexanalyzer-web/commit/2ad891fa6e2b5bd315779d070b40373fbee86fd7))

## [1.0.16](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.15...1.0.16) (2024-12-25)

## [1.0.15](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.14...1.0.15) (2024-12-19)

## [1.0.14](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.13...1.0.14) (2024-12-18)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.15 ([12ba048](https://gitlab.com/appframework/hexanalyzer-web/commit/12ba0480fe9d2d19566b9a4c4d3c6df00c106ca6))

## [1.0.13](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.12...1.0.13) (2024-12-14)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.14 ([baf7f43](https://gitlab.com/appframework/hexanalyzer-web/commit/baf7f4359a20d37332b8bca5bf358282ccb953b3))

## [1.0.12](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.11...1.0.12) (2024-12-11)


### Bug Fixes

* **deps:** update dependency @vueuse/core to v12 ([5fafeb3](https://gitlab.com/appframework/hexanalyzer-web/commit/5fafeb3441a01e52848f0e74eac1222703443bf8))
* **deps:** update dependency bootstrap-vue-next to v0.26.11 ([e38cb96](https://gitlab.com/appframework/hexanalyzer-web/commit/e38cb9672b6b31d3428723bf3c9a8c1b698f18e4))
* **deps:** update dependency vue-router to v4.5.0 ([c37f6c2](https://gitlab.com/appframework/hexanalyzer-web/commit/c37f6c27a4288c164ba98d82d286a0fd6e14b5cb))
* More improvements to get the hex view running ([a3ad3d7](https://gitlab.com/appframework/hexanalyzer-web/commit/a3ad3d78cab51b232dea55fcb316032e0ca69554))

## [1.0.11](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.10...1.0.11) (2024-12-04)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.8 ([ce442ad](https://gitlab.com/appframework/hexanalyzer-web/commit/ce442ad243d642c86631f01fc31ff54a19aa6bff))
* **deps:** update dependency vue-i18n to v10.0.5 ([fd1dca0](https://gitlab.com/appframework/hexanalyzer-web/commit/fd1dca017d33cb98aee1c67829ede66b1883cbcb))

## [1.0.10](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.9...1.0.10) (2024-12-02)

## [1.0.9](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.8...1.0.9) (2024-11-28)

## [1.0.8](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.7...1.0.8) (2024-11-27)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to v0.26.5 ([2ffb316](https://gitlab.com/appframework/hexanalyzer-web/commit/2ffb3167cba08365f9018fa83011bfc9114fbbbe))
* **deps:** update dependency primelocale to v1.2.2 ([778e425](https://gitlab.com/appframework/hexanalyzer-web/commit/778e4252107f9c54a7eef368d78d9697c3b28618))

## [1.0.7](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.6...1.0.7) (2024-11-21)


### Bug Fixes

* **deps:** update dependency vue to v3.5.13 ([ddd322d](https://gitlab.com/appframework/hexanalyzer-web/commit/ddd322dc245390607bc721e5d6ab2cc6a42c6d41))

## [1.0.6](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.5...1.0.6) (2024-11-20)

## [1.0.5](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.4...1.0.5) (2024-11-20)


### Bug Fixes

* **deps:** update dependency bootstrap-vue-next to ^0.26.0 ([a716dab](https://gitlab.com/appframework/hexanalyzer-web/commit/a716dab0149a098f2e77a7d78fccbaa393441752))
* **deps:** update dependency primelocale to v1.2.0 ([af3adec](https://gitlab.com/appframework/hexanalyzer-web/commit/af3adec2926f4d6dabb7607dcb1526cf486294fa))

## [1.0.4](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.3...1.0.4) (2024-11-19)


### Bug Fixes

* Repair parts of the analysis viewer ([350e5cb](https://gitlab.com/appframework/hexanalyzer-web/commit/350e5cbf08d5ab03115d637a8d160e65b939384a))

## [1.0.3](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.2...1.0.3) (2024-11-15)

## [1.0.2](https://gitlab.com/appframework/hexanalyzer-web/compare/1.0.1...1.0.2) (2024-10-24)

## [1.0.1](https://gitlab.com/appframework/hexanalyzer-web/compare/v1.0.0...1.0.1) (2024-10-22)

# 1.0.0 (2024-10-09)


### Features

* **ci:** Add .gitlab-ci.yml ([e6c04ea](https://gitlab.com/appframework/hexanalyzer-web/commit/e6c04ea3b8872ee0fd7080f1ae64b712cd0bb9cb))
* **ci:** Use default quarkus pipeline ([c544f8f](https://gitlab.com/appframework/hexanalyzer-web/commit/c544f8f9236ff91f3c7c543aa23fece13345241f))
* Minor UX improvements ([5f97b62](https://gitlab.com/appframework/hexanalyzer-web/commit/5f97b624e0a9de9bb5a280b42f999d5211123e8b))
* Update to latest appframework ([7cebef0](https://gitlab.com/appframework/hexanalyzer-web/commit/7cebef02436fdcded8c51ab8456e8e738c3a5208))
