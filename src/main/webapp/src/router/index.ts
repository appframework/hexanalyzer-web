import { createRouter, createWebHistory } from 'vue-router'
import { search } from '@/App.vue'
import { isLoggedIn } from '@/store'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/login',
      component: () => import('../appframework/views/LoginView.vue')
    },
    {
      path: '/logout',
      component: () => import('../appframework/views/LogoutView.vue')
    },
    {
      path: '/roles',
      component: () => import('../appframework/views/RolesView.vue')
    },
    {
      path: '/roles/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/roles/clone/:id',
      component: () => import('../appframework/views/RoleView.vue')
    },
    {
      path: '/users',
      component: () => import('../appframework/views/UsersView.vue')
    },
    {
      path: '/users/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/users/clone/:id',
      component: () => import('../appframework/views/UserView.vue')
    },
    {
      path: '/plugins',
      component: () => import('../views/PluginsView.vue')
    },
    {
      path: '/plugins/:id',
      component: () => import('../views/PluginView.vue')
    },
    {
      path: '/plugins/clone/:id',
      component: () => import('../views/PluginView.vue')
    },
    {
      path: '/groups',
      component: () => import('../views/GroupsView.vue')
    },
    {
      path: '/groups/:id',
      component: () => import('../views/GroupView.vue')
    },
    {
      path: '/groups/clone/:id',
      component: () => import('../views/GroupView.vue')
    },
    {
      path: '/binaries',
      component: () => import('../views/BinariesView.vue')
    },
    {
      path: '/binaries/:id',
      component: () => import('../views/BinaryView.vue')
    },
    {
      path: '/binaries/clone/:id',
      component: () => import('../views/BinaryView.vue')
    },
    {
      path: '/binaries/:id/analyze',
      component: () => import('../views/AnalysesView.vue')
    },
    {
      path: '/profile',
      component: () => import('../appframework/views/ProfileView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  // Not logged in, go to login page
  if (!isLoggedIn() && to.path !== '/login') {
    return { path: '/login' }
  }

  // Clear search between modules
  if (to.path === from.path) {
    return
  }
  let currentModule = from.path.split('/')[1]
  if (currentModule === '') {
    currentModule = 'tasks'
  }
  let nextModule = to.path.split('/')[1]
  if (nextModule === '') {
    nextModule = 'tasks'
  }
  if (currentModule !== nextModule) {
    search.value = ''
  }
  return true
})

export default router
