/* tslint:disable */
/* eslint-disable */

export interface AbstractDto {
    id: number;
}

export interface BinaryData extends AbstractDto {
    filename: string;
    size: number;
    contentType: string;
}

export interface Credentials {
    username: string;
    password: string;
}

export interface DeletionAction {
    effect: string;
    action: string;
    affected: number;
}

export interface ImportResult {
    skipped: number;
    created: number;
    updated: number;
    deleted: number;
}

export interface Problem {
    title: string;
    status: number;
    detail: string;
}

export interface Progress<T> {
    key: string;
    result: T;
    value: number;
    max: number;
    message: string;
    completed: boolean;
    success: boolean;
}

export interface Role extends AbstractDto {
    name: string;
    permissions: string[];
}

export interface User extends AbstractDto {
    name: string;
    login: string;
    password: string;
    email: string;
    /**
     * @deprecated
     */
    roleIds: number[];
    roles: string[];
    pictureId: number;
}

export interface VersionInformation {
    maven: string;
    git: string;
    buildTimestamp: Date;
}

export interface Analysis extends AbstractDto {
    binaryId: number;
    name: string;
    start: string;
    end: string;
    description: string;
    color: string;
    foregroundColor: string;
}

export interface Binary extends AbstractDto {
    name: string;
    data: string;
    analyses: Analysis[];
    groupIds: number[];
    status: AnalysisStatus;
    parentId: number;
}

export interface DataSearchResult {
    data: string;
    binaries: Binary[];
}

export interface Group extends AbstractDto {
    name: string;
    description: string;
}

export interface Plugin extends AbstractDto {
    qualifiedName: string;
    source: string;
    compiled: boolean;
    extensionPoint: ExtensionPoint;
}

export type AnalysisStatus = "NOT" | "AUTO" | "PARTIAL" | "COMPLETE";

export type ExtensionPoint = "CREATE_AUTO_ANALYZE";
