/// <reference path="../../../../../../../../node_modules/@types/angular/index.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('hexanalyzerApp')
        .controller('AnalysisOthersController', AnalysisOthersController);

    AnalysisOthersController.$inject = ['$uibModalInstance', 'data', 'start', 'end', 'groupIds', '$http', 'parentScope', 'Group', 'ourValue', 'analysis', '$location'];

    /**
     * @param {ng.IHttpService} $http
     */
    function AnalysisOthersController($uibModalInstance, data, start, end, groupIds, $http, parentScope, Group, ourValue, analysis, $location) {
        /* jshint validthis: true */
        const vm = this;

        vm.data = data;
        vm.cancel = cancel;
        vm.ok = ok;
        vm.reload = reload;
        vm.switchToBinary = switchToBinary;
        vm.switchToAnalysis = switchToAnalysis;
        vm.convertToString = convertToString;

        vm.groups = [];
        vm.selectedGroupIds = [];
        for (const groupId of groupIds) {
            vm.selectedGroupIds[groupId] = true;
        }
        vm.useDataFromAnalysis = useDataFromAnalysis;
        vm.ourValue = ourValue;

        if (analysis) {
            vm.confirmButtons = [{ value: 'ok', label: 'OK' }, { value: 'publish', label: 'Publish' }];
        } else {
            vm.cancelButtons = [];
        }
        vm.cancelButtons = [{ value: 'close', label: 'Close' }];

        activate();

        function activate() {
            Group.query(function(groups_) {
                vm.groups = groups_;
                updateTitle();
            });
        }

        function ok(value) {
            if (value === 'publish') {
                let url = 'rest/analysis/publish?start=' + start + '&end=' + end;
                for (const group of vm.groups) {
                    if (vm.selectedGroupIds[group.id]) {
                        url += '&groupIds=' + group.id;
                    }
                }
                $http.put(url, analysis, {headers: {'Content-Type': 'application/json'}}).then(function() {
                    updateTitle();
                    $uibModalInstance.dismiss(value);
                });
            } else {
                $uibModalInstance.dismiss(value);
            }
        }

        function cancel(value) {
            $uibModalInstance.dismiss(value);
        }

        function reload() {
            let url = 'rest/binary/extract?start=' + start + '&end=' + end;
            for (const group of vm.groups) {
                if (vm.selectedGroupIds[group.id]) {
                    url += '&groupIds=' + group.id;
                }
            }
            $http.get(url).then(function(response) {
                console.log(response.data);
                vm.data = response.data;
                updateTitle();
            });
        }

        function updateTitle() {
            vm.title = vm.data.length + ' different values in 0x' + start.toString(16) + ' - 0x' + end.toString(16);
            let groupNames = '';
            for (const group of vm.groups) {
                if (vm.selectedGroupIds[group.id]) {
                    groupNames += group.name;
                }
            }
            if (groupNames) {
                vm.title += ' in groups ' + groupNames;
            }
        }

        function useDataFromAnalysis(analysis) {
            parentScope.analysis.name = analysis.name;
            parentScope.analysis.description = analysis.description;
            parentScope.analysis.color = analysis.color;
            parentScope.analysis.foregroundColor = analysis.foregroundColor;
            $uibModalInstance.dismiss('ok');
        }

        function switchToBinary(binary) {
            $uibModalInstance.dismiss('ok');
            $location.path('/binaries/' + binary.id);
        }

        function switchToAnalysis(binary) {
            $uibModalInstance.dismiss('ok');
            $location.path('/binaries/' + binary.id + '/analyze');
        }

        function convertToString(data) {
            data = data.replaceAll(' ', '');
            let str = '';
            for (let i = 0; i < data.length; i+=2) {
                let tmp = parseInt(data.substr(i, 2), 16);
                if (tmp !== 0) {
                    if (tmp < 32 || (tmp > 126 && tmp < 161)) {
                        tmp = '.';
                    } else {
                        tmp = String.fromCharCode(tmp);
                    }
                    str += tmp;
                }
            }
            str = str.trim();
            return str;
        }

    }

})();
