/// <reference path="../../../../../../typings/index.d.ts" />

function padLeadingZeros(number, width) {
    let tmp = '' + number;
    while (tmp.length < width) {
        tmp = '0' + tmp;
    }
    return tmp;
}

function integerToHexString(integer) {
    return padLeadingZeros(integer.toString(16).toUpperCase(), 4);
}

(function() {
    'use strict';

    angular.module('hexanalyzerApp')
        .controller('AnalysisBulkCreateController', AnalysisBulkCreateController);

    AnalysisBulkCreateController.$inject = ['$uibModalInstance', '$route', 'binary', 'Analysis', '$q'];

    function AnalysisBulkCreateController($uibModalInstance, $route, binary, Analysis, $q) {
        /* jshint validthis: true */
        const vm = this;

        vm.confirmButtons = [{ value: 'save', label: 'Save' }];
        vm.cancelButtons = [{ value: 'cancel', label: 'Cancel' }];
        vm.binary = binary;
        vm.analysis = {};
        vm.search = '';

        vm.find = find;
        vm.ok = ok;
        vm.cancel = cancel;

        activate();

        function activate() {
            vm.analysis.binaryId = vm.binary.id;
        }

        function find() {
            if (vm.search === '') {
                return [];
            }
            const re = new RegExp(vm.search, 'g');
            let match;
            const result = [];

            while ((match = re.exec(binary.data))) {
                result.push({'start': match.index, 'end': match.index + vm.search.length});
            }
            return result;
        }

        function ok(value) {
            const promises = [];
            for (const element of find()) {
                const analysis = new Analysis(vm.analysis);
                analysis.start = integerToHexString(element.start / 2);
                analysis.end = integerToHexString((element.end - 2) / 2);
                analysis.name += analysis.start + '_' + analysis.end;
                promises.push(analysis.$save().$promise);
            }
            $q.all(promises).then(function() {
                $route.reload();
                $uibModalInstance.dismiss(value);
            });
        }

        function cancel(value) {
            $uibModalInstance.dismiss(value);
        }
    }
})();
