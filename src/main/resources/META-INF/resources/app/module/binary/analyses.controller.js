/// <reference path="../../../../../../../../node_modules/@types/angular/index.d.ts" />
/// <reference path="analysis.factory.d.ts" />
/// <reference path="binary.factory.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('hexanalyzerApp')
        .controller('AnalysesController', AnalysesController);

    AnalysesController.$inject = ['$uibModal', '$route', '$http', '$location', 'Binary', 'Analysis'];

    /**
     * @param {ng.ui.bootstrap.IModalService} $uibModal
     * @param {ng.route.IRouteService} $route
     * @param {ng.IHttpService} $http
     * @param {ng.ILocationService} $location
     * @param {hexanalyzer.Binary} Binary
     * @param {hexanalyzer.Analysis} Analysis
     */
    function AnalysesController($uibModal, $route, $http, $location, Binary, Analysis) {
        /* jshint validthis: true */
        const vm = this;

        vm.analysisLength = analysisLength;
        vm.editAnalysis = editAnalysis;
        vm.splitAnalysis = splitAnalysis;
        vm.showOthers = showOthers;
        vm.removeAnalysis = removeAnalysis;
        vm.partial = partial;
        vm.complete = complete;
        vm.not = not;
        vm.splitByRoots = splitByRoots;
        vm.bulkCreate = bulkCreate;
        vm.edit = edit;

        activate();

        function analysisLength(analysis) {
            return parseInt(analysis.end, 16) - parseInt(analysis.start, 16) + 1;
        }

        function activate() {
            Binary.get({id: $route.current.params.id}).$promise.then(function(binary) {
                vm.binary = binary;
                renderPacket();
            });
        }

        function generateCss(name, color, foregroundColor) {
            return '.' + name + ' { margin-left: -1px; margin-right: -1px; border: 1px solid ' + color + '; color: ' + color + '; transition: all 0.1s ease; }\n' +
            '.' + name + '_hovered { background-color: ' + color + '; opacity: 0.4; color: ' + foregroundColor + '; }\n';
        }

        function registerHovering(element) {
            $(element).on('mouseover', function(event) {
                $($(this).attr('class').split(' ')).each(function(index1, element1) {
                    if (element1.indexOf('hover') === -1) {
                        $('.' + element1).each(function(index2, element2) {
                            $(element2).addClass(element1 + '_hovered');
                        });
                    }
                });
                event.stopPropagation();
            });
            $(element).on('mouseout', function(event) {
                $($(this).attr('class').split(' ')).each(function(index1, element1) {
                    if (element1.indexOf('hover') === -1) {
                        $('.' + element1).each(function(index2, element2) {
                            $(element2).removeClass(element1 + '_hovered');
                        });
                    }
                });
                event.stopPropagation();
            });
        }

        let hex_element_start;
        function renderPacket() {

            // Generate CSS classes for all known analysis entries
            vm.cssMap = {};
            $(vm.binary.analyses).each(function(index, element) {
                if (element.color) {
                    const foregroundColor = element.foregroundColor && element.foregroundColor !== '' ? element.foregroundColor : '#000000';
                    vm.cssMap[element.name] = [element.color, foregroundColor];
                }
            });
            let generatedCss = '<style type="text/css">';
            for (const name in vm.cssMap) {
                const element = vm.cssMap[name];
                generatedCss += generateCss(name, element[0], element[1]);
            }
            $(generatedCss + '</style>').appendTo('head');

            let position, offset, hexadecimal, decoded;
            offset = 0;
            position = '<div class="display_header">Offset(h)</div><div class="display_header">' + padLeadingZeros(offset, 8) + '</div>';
            hexadecimal = '<div class="display_header">00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F</div>';
            decoded = '<br/>';
            vm.legend = '';
            for (let i = 0; i < vm.binary.data.length; i+=2) {
                const hexOffset = integerToHexString(i / 2);

                // Check if the current byte is a starting byte of any analysis entry
                $(vm.binary.analyses).each(function(index, element) {
                    if (parseInt(element.start, 16) * 2 === i) {
                        hexadecimal += '<span class="hoverable ' + element.name + '">';
                        decoded += '<span class="hoverable ' + element.name + '">';
                        vm.legend += '<div class="hoverable ' + element.name + '">' + (element.description ? element.description : element.name) + ' (' + analysisLength(element) + ' Bytes)' +
                            ' <a ng-click="vm.editAnalysis(' + element.id + ')" title="edit"><i class="fa fa-pencil"></i></a>' +
                            ' <a ng-click="vm.showOthers(' + parseInt(element.start, 16) + ',' + parseInt(element.end, 16) + ',' + JSON.stringify(vm.binary.groupIds) + ',' + element.id + ')" title="show others"><i class="fa fa-search"></i></a>' +
                            ' <a ng-click="vm.removeAnalysis(' + element.id + ')" title="delete"><i class="fa fa-trash-o"></i></a>' +
                            ' <a ng-click="vm.splitAnalysis(' + element.id + ')" title="split"><i class="fa fa-scissors"></i></a>';
                    }
                });

                // Render the bytes
                hexadecimal += '<span class="decoded_or_hex_element" id="hex_element_' + hexOffset + '">' + vm.binary.data.substr(i, 2) + '</span>';

                // Render the decoded bytes
                let tmp = parseInt(vm.binary.data.substr(i, 2), 16);
                if (tmp < 32 || (tmp > 126 && tmp < 161)) {
                    tmp = '.';
                } else {
                    tmp = String.fromCharCode(tmp);
                }
                decoded += '<span class="decoded_or_hex_element" id="decoded_element_' + hexOffset + '">' + tmp + '</span>';

                $(vm.binary.analyses).each(function(index, element) {
                    if (parseInt(element.end, 16) *2 === i) {
                        hexadecimal += '</span>';
                        decoded += '</span>';
                        vm.legend += '</div>';
                    }
                });

                hexadecimal += ' ';

                // Add line breaks where necessary
                if (i % 32 === 30) {
                    offset += 16;
                    position += '<div class="display_header">' + padLeadingZeros(offset.toString(16).toUpperCase(), 8) + '</div>';
                    hexadecimal += '<br/>';
                    decoded += '<br/>';
                }

            }

            $('#position').html(position);
            $('#hexadecimal').html(hexadecimal);
            $('#decoded').html(decoded);

            $('.hoverable').each(function(index, element) {
                registerHovering(element);
            });
            $('.decoded_or_hex_element').click(function() {
                let hex_element_current;
                if (this.id.indexOf('hex_element_') === 0) {
                    hex_element_current = this.id.substring(12);
                } else if (this.id.indexOf('decoded_element_') === 0) {
                    hex_element_current = this.id.substring(16);
                } else {
                    return;
                }
                $('#hex_element_' + hex_element_current).addClass('decoded_or_hex_element_selected');
                $('#decoded_element_' + hex_element_current).addClass('decoded_or_hex_element_selected');
                if (!hex_element_start) {
                    hex_element_start = hex_element_current;
                } else {
                    // Make sure hex_element_start is the first element
                    if (hex_element_current < hex_element_start) {
                        const tmp = hex_element_current;
                        hex_element_current = hex_element_start;
                        hex_element_start = tmp;
                    }

                    const analysis = new Analysis();
                    analysis.name = '';
                    analysis.start = hex_element_start;
                    analysis.end = hex_element_current;
                    analysis.description = '';
                    analysis.color = '';
                    analysis.foregroundColor = '';
                    analysis.binaryId = vm.binary.id;

                    $uibModal.open({
                        templateUrl: 'app/module/binary/analysis.html',
                        controller: 'AnalysisController',
                        controllerAs: 'vm',
                        resolve: {
                            parentScope: vm,
                            analysis: analysis,
                            binary: vm.binary
                        }
                    });

                    $('#hex_element_' + hex_element_start).removeClass('decoded_or_hex_element_selected');
                    $('#hex_element_' + hex_element_current).removeClass('decoded_or_hex_element_selected');
                    $('#decoded_element_' + hex_element_start).removeClass('decoded_or_hex_element_selected');
                    $('#decoded_element_' + hex_element_current).removeClass('decoded_or_hex_element_selected');
                    hex_element_start = undefined;
                }
            });

        }

        function editAnalysis(analysisId) {
            for (const analysis of vm.binary.analyses) {
                if (analysis.id === analysisId) {
                    $uibModal.open({
                        templateUrl: 'app/module/binary/analysis.html',
                        controller: 'AnalysisController',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            parentScope: vm,
                            analysis: analysis,
                            binary: vm.binary
                        }
                    });
                }
            }
        }

        function splitAnalysis(analysisId) {

            let binary;
            for (const analysis of vm.binary.analyses) {
                if (analysis.id === analysisId) {
                    binary = new Binary();
                    binary.status = vm.binary.status;
                    binary.name = vm.binary.name + ' (' + analysis.start + ' -> ' + analysis.end + ')';
                    binary.data = vm.binary.data.substring(parseInt(analysis.start, 16) * 2, (parseInt(analysis.end, 16) + 1) * 2);
                    binary.groupIds = vm.binary.groupIds;
                    binary.analyses = [angular.copy(analysis)];
                    binary.parentId = analysis.id;
                    break;
                }
            }

            if (binary) {
                Binary.query({'search': binary.name}, function(data) {
                    if (data.length == 0) {
                        const startWithin = parseInt(binary.analyses[0].start, 16);
                        const endWithin = parseInt(binary.analyses[0].end, 16);
                        for (const analysis of vm.binary.analyses) {
                            const start = parseInt(analysis.start, 16);
                            const end = parseInt(analysis.end, 16);
                            if (start >= startWithin && end <= endWithin && binary.analyses[0].id != analysis.id) {
                                const v = angular.copy(analysis);
                                binary.analyses.push(v);
                            }
                        }
                        for (const analysis of binary.analyses) {
                            analysis.start = integerToHexString(parseInt(analysis.start, 16) - startWithin);
                            analysis.end = integerToHexString(parseInt(analysis.end, 16) - startWithin);
                            delete analysis.id;
                            delete analysis.binaryId;
                        }
                        binary.$save(function(binary) {
                            //$location.path('/binaries/' + binary.id + '/analyze');
                            toastr.success('', 'Saved as ' + binary.id, {timeOut: 5000, closeButton: true});
                        });
                    } else {
                        toastr.success('', 'Already exists', {timeOut: 5000, closeButton: true});
                    }
                });

            }
        }

        function bulkCreate() {
            $uibModal.open({
                templateUrl: 'app/module/binary/analysis_bulk.html',
                controller: 'AnalysisBulkCreateController',
                controllerAs: 'vm',
                resolve: {
                    binary: vm.binary
                }
            });
        }

        function showOthers(start, end, groupIds, analysisId) {
            let analysis_;
            for (const analysis of vm.binary.analyses) {
                if (analysis.id === analysisId) {
                    analysis_ = analysis;
                    break;
                }
            }

            let url = 'rest/binary/extract?start=' + start + '&end=' + end;
            for (const groupId of groupIds) {
                url += '&groupIds=' + groupId;
            }
            $http.get(url).then(function(response) {
                const data = response.data;
                $uibModal.open({
                    templateUrl: 'app/module/binary/analysis_others.html',
                    controller: 'AnalysisOthersController',
                    controllerAs: 'vm',
                    size: 'xl',
                    resolve: {
                        parentScope: vm,
                        data: function() { return data; },
                        start: function() { return start; },
                        end: function() { return end; },
                        ourValue: function() { return vm.binary.data.substring(start*2, (end+1)*2); },
                        groupIds: function() { return groupIds; },
                        analysis: function() { return analysis_; }
                    }
                });
            });
        }

        function removeAnalysis(analysisId) {
            for (const analysis of vm.binary.analyses) {
                if (analysis.id === analysisId) {
                    new Analysis(analysis).$delete(function() {
                        $route.reload();
                    });
                }
            }
        }

        function edit() {
            $location.path('/binaries/' + vm.binary.id);
        }

    }

})();
