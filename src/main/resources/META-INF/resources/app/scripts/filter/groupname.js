/// <reference path="../../../../../../typings/globals/angular/index.d.ts" />
(function() {

    angular
        .module('hexanalyzerApp')
        .filter('groupname', groupname);

    function groupname() {
        return function(input, groups) {
            if (typeof groups === 'undefined') {
                return '';
            }
            if (!input) {
                return '';
            }
            const result = groups[input];
            if (!result) {
                return 'Unknown';
            }
            return result.name;
        };
    }

})();
