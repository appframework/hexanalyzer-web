/// <reference path="../../../../../../../node_modules/@types/angular/index.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('hexanalyzerApp')
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider'];
    run.$inject = ['$rootScope', '$location'];

    /**
     * @param {angular.route.IRouteProvider} $routeProvider
     */
    function config($routeProvider) {

        /**
         * @param {string} route
         * @param {string} templateUrl
         * @param {string} controller
         */
        function addRoute(route, templateUrl, controller) {
            $routeProvider.when(route, {
                templateUrl: templateUrl,
                controller: controller,
                controllerAs: 'vm'
            });
        }

        addRoute('/', 'app/module/home/home.html', 'HomeController');

        addRoute('/binaries', 'app/module/binary/binaries.html', 'BinaryListController');
        addRoute('/binaries/:id', 'app/module/binary/binary.html', 'BinaryDetailController');
        addRoute('/binaries/:id/analyze', 'app/module/binary/analyses.html', 'AnalysesController');

        addRoute('/groups', 'app/module/group/groups.html', 'GroupListController');
        addRoute('/groups/:id', 'app/module/group/group.html', 'GroupDetailController');

        addRoute('/plugins', 'app/module/plugin/plugins.html', 'PluginListController');
        addRoute('/plugins/:id', 'app/module/plugin/plugin.html', 'PluginDetailController');
    }

    /**
     * @param {appframework.IAppframeworkRootScopeService} $rootScope
     * @param {ng.ILocationService} $location
     */
    function run($rootScope, $location) {
        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            const base = $location.absUrl().replace($location.url(), '');
            if (base === current || next === current) {
                return;
            }
            let currentModule = current.substring(base.length + 1).split('/')[0];
            if (currentModule === '') { currentModule = 'tasks'; }
            let nextModule = next.substring(base.length + 1).split('/')[0];
            if (nextModule === '') { nextModule = 'tasks'; }
            if (currentModule !== nextModule) {
                $rootScope.search = '';
            }
        });
    }

})();
