/// <reference path="../../../../../../../node_modules/@types/angular/index.d.ts" />
/// <reference path="../../../../../../../node_modules/appframework/index.d.ts" />
// @ts-check

(function() {
    angular
        .module('hexanalyzerApp')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$location', '$http', '$scope', '$rootScope'];

    /**
     * @param {ng.ILocationService} $location
     * @param {ng.IHttpService} $http
     * @param {ng.IScope} $scope
     * @param {af.IAppframeworkRootScopeService} $rootScope
     */
    function MainCtrl($location, $http, $scope, $rootScope) {
        /* jshint validthis: true */
        /** @type {t.controller.MainCtrl} */
        const vm = this;

        vm.permissions = [];

        vm.currentModule = currentModule;
        vm.hasPermission = hasPermission;
        vm.hasAnyPermission = hasAnyPermission;
        vm.isLoggedIn = isLoggedIn;
        vm.getVersion = getVersion;

        activate();

        function activate() {
            $scope.$watch('state.user', function(newValue) {
                if (newValue !== undefined && newValue.username !== undefined) {
                    vm.permissions = $rootScope.state.permissions;
                    $http.get('rest/version_info').then(function(response) {
                        vm.version = response.data;
                    });
                } else {
                    vm.permissions = [];
                }
            });
        }

        function currentModule() {
            let currentPath = $location.path();
            if (currentPath.startsWith('/')) {
                currentPath = currentPath.substring(1);
            }
            const firstSlash = currentPath.indexOf('/');
            if (firstSlash >= 0) {
                currentPath = currentPath.substring(0, firstSlash);
            }
            return currentPath;
        }

        function hasPermission(permission) {
            return $.inArray(permission, vm.permissions) >= 0;
        }

        function hasAnyPermission() {
            for (let i = 0; i < arguments.length; i++) {
                if ($.inArray(arguments[i], vm.permissions) >= 0) {
                    return true;
                }
            }
            return false;
        }

        function isLoggedIn() {
            return angular.isDefined($rootScope.state.user.username);
        }

        function getVersion() {
            if (vm.version) {
                let version = vm.version.maven;
                if (version.indexOf('-SNAPSHOT', version.length - '-SNAPSHOT'.length) !== -1) {
                    version += ' (' + vm.version.git + ')';
                }
                if (vm.version.buildTimestamp) {
                    version += ' - ' + dayjs(vm.version.buildTimestamp).format('LLL');
                }
                return version;
            }
        }

    }
})();
