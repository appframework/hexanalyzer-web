/// <reference path="../../../../../../../node_modules/@types/angular/index.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('hexanalyzerApp', ['ui.bootstrap', 'ngSanitize', 'angularjs-dropdown-multiselect', 'appframework'])
        .config(['$locationProvider', config]);

    /**
     * @param {ng.ILocationService} $locationProvider
     */
    function config($locationProvider) {
        $locationProvider.hashPrefix('');
    }
})();

