CREATE TABLE binary_ (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  data text NOT NULL,
  name varchar(255) NOT NULL,
  group_id int DEFAULT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE binary_
ADD CONSTRAINT FK_binary_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE binary_
ADD CONSTRAINT FK_binary_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE TABLE analysis (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  description varchar(255),
  start int NOT NULL,
  "end" int NOT NULL,
  color varchar(255),
  foreground_color varchar(255),
  binary_id int NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE analysis
ADD CONSTRAINT FK_analysis_binary FOREIGN KEY (binary_id) REFERENCES binary_ (id);
ALTER TABLE analysis
ADD CONSTRAINT FK_analysis_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE analysis
ADD CONSTRAINT FK_analysis_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
