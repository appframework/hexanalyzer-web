CREATE TABLE plugin (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  qualifiedName varchar(255) NOT NULL,
  source text NOT NULL,
  compiled bytea,
  extensionPoint varchar(255) NOT NULL,
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE plugin
ADD CONSTRAINT FK_plugin_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE plugin
ADD CONSTRAINT FK_plugin_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);
