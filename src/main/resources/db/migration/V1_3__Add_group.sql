CREATE TABLE group_ (
  id serial NOT NULL,
  created timestamp DEFAULT NULL,
  modified timestamp DEFAULT NULL,
  name varchar(255) NOT NULL,
  description varchar(255),
  creator_id int DEFAULT NULL,
  modificator_id int DEFAULT NULL,
  PRIMARY KEY(id)
);

ALTER TABLE group_
ADD CONSTRAINT FK_group_creator FOREIGN KEY (creator_id) REFERENCES user_ (id);
ALTER TABLE group_
ADD CONSTRAINT FK_group_modificator FOREIGN KEY (modificator_id) REFERENCES user_ (id);

CREATE TABLE binary_group (
  group_id int NOT NULL,
  binary_id int NOT NULL,
  PRIMARY KEY(group_id, binary_id)
);
