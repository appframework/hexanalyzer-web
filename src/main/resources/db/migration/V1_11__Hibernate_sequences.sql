create sequence analysis_SEQ INCREMENT 50;
select setval('analysis_SEQ', (select max(id) from analysis));

create sequence binary__SEQ INCREMENT 50;
select setval('binary__SEQ', (select max(id) from binary_));

create sequence binary_data_SEQ INCREMENT 50;
select setval('binary_data_SEQ', (select max(id) from binary_data));

create sequence group__SEQ INCREMENT 50;
select setval('group__SEQ', (select max(id) from group_));

create sequence plugin_SEQ INCREMENT 50;
select setval('plugin_SEQ', (select max(id) from plugin));

create sequence role_SEQ INCREMENT 50;
select setval('role_SEQ', (select max(id) from role));

create sequence user__SEQ INCREMENT 50;
select setval('user__SEQ', (select max(id) from user_));

drop sequence if exists hibernate_sequence;