ALTER TABLE binary_
  ADD COLUMN parent_id int DEFAULT NULL;
ALTER TABLE binary_
  ADD CONSTRAINT FK_binarydata_parent FOREIGN KEY (parent_id) REFERENCES analysis (id);
CREATE INDEX FK_binarydata_parent_IX ON binary_ (parent_id);