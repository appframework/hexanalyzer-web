ALTER TABLE binary_
  ADD COLUMN status VARCHAR(50) DEFAULT 'NOT';

UPDATE binary_
  SET status = 'NOT';
