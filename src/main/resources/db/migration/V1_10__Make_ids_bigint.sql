ALTER TABLE analysis
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN binary_id TYPE bigint;
ALTER TABLE binary_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN group_id TYPE bigint,
    ALTER COLUMN parent_id TYPE bigint;
ALTER TABLE binary_data
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE binary_group
    ALTER COLUMN group_id TYPE bigint,
    ALTER COLUMN binary_id TYPE bigint;
ALTER TABLE group_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE plugin
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint;
ALTER TABLE role_permissions
    ALTER COLUMN role_id TYPE bigint;
ALTER TABLE user_
    ALTER COLUMN id TYPE bigint,
    ALTER COLUMN creator_id TYPE bigint,
    ALTER COLUMN modificator_id TYPE bigint,
    ALTER COLUMN picture_id TYPE bigint;
ALTER TABLE user_role
    ALTER COLUMN user_id TYPE bigint,
    ALTER COLUMN role_id TYPE bigint;
