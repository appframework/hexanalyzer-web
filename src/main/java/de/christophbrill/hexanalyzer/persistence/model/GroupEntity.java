/*
 * Copyright 2013  Christoph Brill <opensource@christophbrill.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.hexanalyzer.persistence.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.hexanalyzer.persistence.selector.GroupSelector;

@Entity(name = "Group")
@Table(name = "group_")
public class GroupEntity extends DbObject {

    private static final long serialVersionUID = 5270941129462964429L;

    public String name;
    public String description;
    @ManyToMany(mappedBy = "groups")
    public List<BinaryEntity> binaries = new ArrayList<>(0);

    public static List<GroupEntity> findByIds(List<Long> ids) {
        return new GroupSelector(getEntityManager()).withIds(ids).findAll();
    }
}
