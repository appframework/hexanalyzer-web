/*
 * Copyright 2013  Christoph Brill <opensource@christophbrill.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.hexanalyzer.persistence.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.hexanalyzer.ui.dto.Binary.AnalysisStatus;

@Entity(name = "Binary")
@Table(name = "binary_")
public class BinaryEntity extends DbObject {

    private static final long serialVersionUID = -5505087291070599590L;

    @Column(length = 16777216)
    public String data;
    public String name;
    @OneToMany(mappedBy = "binary", cascade = CascadeType.ALL)
    @OrderBy("start asc, end_ desc")
    public List<AnalysisEntity> analyses = new ArrayList<>(0);
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "binary_group", joinColumns = {
            @JoinColumn(name = "binary_id", nullable = false) }, inverseJoinColumns = {
            @JoinColumn(name = "group_id", nullable = false)
    })
    @OrderBy("name")
    public List<GroupEntity> groups = new ArrayList<>(0);
    @Enumerated(EnumType.STRING)
    public AnalysisStatus status;
    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    public AnalysisEntity parent;

}
