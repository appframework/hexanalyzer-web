package de.christophbrill.hexanalyzer.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity_;

public class AnalysisSelector extends AbstractResourceSelector<AnalysisEntity> {

	private String search;

	public AnalysisSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<AnalysisEntity> getEntityClass() {
		return AnalysisEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
													@Nonnull Root<AnalysisEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(
					builder.or(
							builder.like(from.get(AnalysisEntity_.description), likePattern),
							builder.like(from.get(AnalysisEntity_.name), likePattern)
					)
			);
		}

		return predicates;
	}

	@Override
	public AnalysisSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
