/*
 * Copyright 2016  Christoph Brill <opensource@christophbrill.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.hexanalyzer.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity_;
import de.christophbrill.hexanalyzer.ui.dto.Plugin.ExtensionPoint;

public class PluginSelector extends AbstractResourceSelector<PluginEntity> {

	private ExtensionPoint extensionPoint;
	private String search;

	public PluginSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<PluginEntity> getEntityClass() {
		return PluginEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
													@Nonnull Root<PluginEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (extensionPoint != null) {
			builder.equal(from.get(PluginEntity_.extensionPoint), extensionPoint);
		}

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(
					builder.or(
							builder.like(from.get(PluginEntity_.qualifiedName), likePattern),
							builder.like(from.get(PluginEntity_.source), likePattern)
					)
			);
		}

		return predicates;
	}

	public PluginSelector withExtensionPoint(ExtensionPoint extensionPoint) {
		this.extensionPoint = extensionPoint;
		return this;
	}

	@Override
	public PluginSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
