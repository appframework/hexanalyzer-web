package de.christophbrill.hexanalyzer.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.model.DbObject_;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity_;

public class BinarySelector extends AbstractResourceSelector<BinaryEntity> {

	private String nameLike;
	private List<Integer> groupIds;
	private String search;

	public BinarySelector(EntityManager em) {
		super(em);
	}

	@Override
	@Nonnull
	protected Class<BinaryEntity> getEntityClass() {
		return BinaryEntity.class;
	}

	@Override
	@Nonnull
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder, @Nonnull Root<BinaryEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (StringUtils.isNotEmpty(nameLike)) {
			predicates.add(builder.like(from.get(BinaryEntity_.name), '%' + nameLike + '%'));
		}

		if (CollectionUtils.isNotEmpty(groupIds)) {
			criteriaQuery.distinct(true);
			predicates.add(from.join(BinaryEntity_.groups).get(DbObject_.id).in(groupIds));
		}

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(
					builder.or(
							builder.like(from.get(BinaryEntity_.name), likePattern)
					)
			);
		}

		return predicates;
	}

	public BinarySelector withNameLike(String nameLike) {
		this.nameLike = nameLike;
		return this;
	}

	public BinarySelector withGroupIds(List<Integer> groupIds) {
		this.groupIds = groupIds;
		return this;
	}

	@Override
	public BinarySelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
