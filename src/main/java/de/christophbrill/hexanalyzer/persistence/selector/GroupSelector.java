package de.christophbrill.hexanalyzer.persistence.selector;

import java.util.List;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity_;

public class GroupSelector extends AbstractResourceSelector<GroupEntity> {

	private String search;

	public GroupSelector(EntityManager em) {
		super(em);
	}

	@Nonnull
	@Override
	protected Class<GroupEntity> getEntityClass() {
		return GroupEntity.class;
	}

	@Nonnull
	@Override
	protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
													@Nonnull Root<GroupEntity> from,
													@Nonnull CriteriaQuery<?> criteriaQuery) {
		List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

		if (StringUtils.isNotEmpty(search)) {
			String likePattern = '%' + search + '%';
			predicates.add(
					builder.or(
							builder.like(from.get(GroupEntity_.name), likePattern),
							builder.like(from.get(GroupEntity_.description), likePattern)
					)
			);
		}

		return predicates;
	}

	@Override
	public GroupSelector withSearch(String search) {
		this.search = search;
		return this;
	}

}
