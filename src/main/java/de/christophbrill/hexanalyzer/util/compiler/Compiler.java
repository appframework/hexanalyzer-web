package de.christophbrill.hexanalyzer.util.compiler;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Compiler {

	private static final Logger LOG = LoggerFactory.getLogger(Compiler.class);
	
	private static final JavaCompiler COMPILER = ToolProvider.getSystemJavaCompiler();
	private static final BufferingJavaFileManager FILE_MANAGER;

	static {
		if (COMPILER != null) {
			FILE_MANAGER = new BufferingJavaFileManager(COMPILER.getStandardFileManager(null, null, null));
		} else {
			LOG.warn("No java compiler available");
			FILE_MANAGER = null;
		}
	}

	public static Class<?> load(@Nonnull String name, @Nonnull byte[] compiled) throws ClassNotFoundException {
		// No file manager, no class
		if (FILE_MANAGER == null) {
			return null;
		}

		FILE_MANAGER.inject(name, compiled);
		return new RuntimeClassLoader(FILE_MANAGER, Thread.currentThread().getContextClassLoader()).loadClass(name);
	}

	@Nullable
	public static byte[] compile(@Nonnull String className, @Nonnull String source) {
		// No compiler, no class
		if (COMPILER == null) {
			return null;
		}

		// Inspired by https://blog.jooq.org/how-to-compile-a-class-at-runtime-with-java-8-and-9/
		Lookup lookup = MethodHandles.lookup();
		try {
			lookup.lookupClass().getClassLoader().loadClass(className);
		} catch (ClassNotFoundException ignore) {
			try {
				compile0(className, source, lookup);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				return null;
			}
		}
		ClassJavaFileObject klass = FILE_MANAGER.getClass(className);
		assert klass != null;
		return klass.getBytes();
	}

	static Class<?> compile0(String className, String content, Lookup lookup) throws Exception {

		List<MemoryJavaFileObject> files = new ArrayList<>();
		files.add(new MemoryJavaFileObject(className, content));

		CompilationTask compilationTask = COMPILER.getTask(null, FILE_MANAGER, null, null, null, files);
		if (!compilationTask.call()) {
			throw new RuntimeException();
		}

		Class<?> caller = StackWalker.getInstance(RETAIN_CLASS_REFERENCE)
				.walk(s -> s.skip(2).findFirst().get().getDeclaringClass());

		Class<?> result = null;
		if (className.startsWith(caller.getPackageName())) {
			result = MethodHandles.privateLookupIn(caller, lookup).defineClass(FILE_MANAGER.getClass(className).getBytes());
		} else {
			result = new ClassLoader() {
				@Override
				protected Class<?> findClass(String name) throws ClassNotFoundException {
					byte[] b = FILE_MANAGER.getClass(name).getBytes();
					int len = b.length;
					return defineClass(className, b, 0, len);
				}
			}.loadClass(className);
		}

		return result;
	}

	private static class MemoryJavaFileObject extends SimpleJavaFileObject {
		private final String source;

		public MemoryJavaFileObject(String className, String source) {
			super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
			this.source = source;
		}

		@Override
		public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
			return source;
		}
	}

	private static class ClassJavaFileObject extends SimpleJavaFileObject {
		private final ByteArrayOutputStream outputStream;

		protected ClassJavaFileObject(String className, Kind kind) {
			super(URI.create("string:///" + className.replace('.', '/') + kind.extension), kind);
			outputStream = new ByteArrayOutputStream();
		}

		@Override
		public OutputStream openOutputStream() throws IOException {
			return outputStream;
		}

		public byte[] getBytes() {
			return outputStream.toByteArray();
		}

	}

	private static class BufferingJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {
		private final Map<String, ClassJavaFileObject> files = new HashMap<>();

		protected BufferingJavaFileManager(JavaFileManager fileManager) {
			super(fileManager);
		}

		@Override
		public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind,
				FileObject sibling) throws IOException {
			ClassJavaFileObject file = new ClassJavaFileObject(className, kind);
			files.put(className, file);
			return file;
		}

		public void inject(String className, byte[] compiled) {
			ClassJavaFileObject file = new ClassJavaFileObject(className, JavaFileObject.Kind.CLASS);
			try (OutputStream out = file.openOutputStream()) {
				out.write(compiled);
			} catch (IOException e) {
				return;
			}
			files.put(className, file);
		}

		@Nullable
		public ClassJavaFileObject getClass(@Nonnull String className) {
			return files.get(className);
		}
	}

	private static class RuntimeClassLoader extends ClassLoader {

		private final BufferingJavaFileManager fileManager;

		private RuntimeClassLoader(BufferingJavaFileManager fileManager, ClassLoader delegate) {
			super(delegate);
			this.fileManager = fileManager;
		}

		@Override
		protected Class<?> findClass(String name) throws ClassNotFoundException {
			assert name != null;
			ClassJavaFileObject file = fileManager.getClass(name);
			if (file != null) {
				byte[] bytes = file.getBytes();
				return super.defineClass(name, bytes, 0, bytes.length);
			}
			return super.findClass(name);
		}
	}
}
