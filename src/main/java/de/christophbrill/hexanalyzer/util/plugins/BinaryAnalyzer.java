package de.christophbrill.hexanalyzer.util.plugins;

import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;

public interface BinaryAnalyzer {

	void analyze(BinaryEntity binary);

}
