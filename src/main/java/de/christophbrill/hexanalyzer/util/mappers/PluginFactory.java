package de.christophbrill.hexanalyzer.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.ui.dto.Plugin;
import org.mapstruct.ObjectFactory;

public class PluginFactory {

    @ObjectFactory
    public PluginEntity createEntity(Plugin dto) {
        if (dto != null && dto.id != null) {
            PluginEntity entity = PluginEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Plugin with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new PluginEntity();
    }
    
}
