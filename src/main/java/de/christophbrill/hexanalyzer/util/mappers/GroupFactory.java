package de.christophbrill.hexanalyzer.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import de.christophbrill.hexanalyzer.ui.dto.Group;
import org.mapstruct.ObjectFactory;

public class GroupFactory {

    @ObjectFactory
    public GroupEntity createEntity(Group dto) {
        if (dto != null && dto.id != null) {
            GroupEntity entity = GroupEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Group with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new GroupEntity();
    }
    
}
