package de.christophbrill.hexanalyzer.util.mappers;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import de.christophbrill.hexanalyzer.ui.dto.Binary;

@Mapper(uses = {BinaryFactory.class, AnalysisMapper.class})
public interface BinaryMapper extends ResourceMapper<Binary, BinaryEntity> {

    BinaryMapper INSTANCE = Mappers.getMapper(BinaryMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "data", ignore = true)
    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "parent", ignore = true)
    BinaryEntity mapDtoToEntity(Binary dto);

    @Override
    @Mapping(target = "data", ignore = true)
    @Mapping(target = "groupIds", ignore = true)
    @Mapping(target = "parentId", ignore = true)
    Binary mapEntityToDto(BinaryEntity entity);

    @AfterMapping
    default void customDtoToEntity(Binary dto, @MappingTarget BinaryEntity entity) {
        if (CollectionUtils.isNotEmpty(dto.groupIds)) {
            entity.groups = GroupEntity.findByIds(dto.groupIds);
        } else {
            entity.groups = null;
        }
        entity.data = dto.data != null ? dto.data.replaceAll("[^0-9a-fA-F]+", "").toLowerCase() : null;
        if (dto.parentId != null) {
            entity.parent = AnalysisEntity.findById(dto.parentId);
        } else {
            entity.parent = null;
        }
    }

    @AfterMapping
    default void customEntityToDto(BinaryEntity entity, @MappingTarget Binary dto) {
        if (CollectionUtils.isNotEmpty(entity.groups)) {
            dto.groupIds = entity.groups.stream().map((g) -> g.id).collect(Collectors.toList());
        } else {
            dto.groupIds = null;
        }
        dto.data = entity.data;
        dto.parentId = (entity.parent != null ? entity.parent.id : null);
    }

}
