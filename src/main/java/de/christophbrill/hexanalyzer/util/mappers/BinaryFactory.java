package de.christophbrill.hexanalyzer.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.ui.dto.Binary;
import org.mapstruct.ObjectFactory;

public class BinaryFactory {

    @ObjectFactory
    public BinaryEntity createEntity(Binary dto) {
        if (dto != null && dto.id != null) {
            BinaryEntity entity = BinaryEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Binary with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new BinaryEntity();
    }
    
}
