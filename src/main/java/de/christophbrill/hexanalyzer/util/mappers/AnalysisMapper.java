package de.christophbrill.hexanalyzer.util.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.ui.dto.Analysis;

@Mapper(uses = AnalysisFactory.class)
public interface AnalysisMapper extends ResourceMapper<Analysis, AnalysisEntity> {

    AnalysisMapper INSTANCE = Mappers.getMapper(AnalysisMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "start", ignore = true)
    @Mapping(target = "end", ignore = true)
    @Mapping(target = "binary", ignore = true)
    @Mapping(target = "children", ignore = true)
    AnalysisEntity mapDtoToEntity(Analysis dto);

    @Override
    @Mapping(target = "start", ignore = true)
    @Mapping(target = "end", ignore = true)
    @Mapping(target = "binaryId", ignore = true)
    Analysis mapEntityToDto(AnalysisEntity entity);

    @AfterMapping
    default void customDtoToEntity(Analysis dto, @MappingTarget AnalysisEntity entity) {
        if (dto.binaryId != null) {
            entity.binary = BinaryEntity.findById(dto.binaryId);
        }
        entity.start = Integer.parseInt(dto.start, 16);
        entity.end = Integer.parseInt(dto.end, 16);
    }

    @AfterMapping
    default void customEntityToDto(AnalysisEntity entity, @MappingTarget Analysis dto) {
        if (entity.binary != null) {
            dto.binaryId = entity.binary.id;
        } else {
            dto.binaryId = null;
        }
        dto.start = Integer.toString(entity.start, 16);
        dto.end = Integer.toString(entity.end, 16);
    }

}
