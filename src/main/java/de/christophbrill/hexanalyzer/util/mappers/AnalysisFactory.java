package de.christophbrill.hexanalyzer.util.mappers;

import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.ui.dto.Analysis;
import org.mapstruct.ObjectFactory;

public class AnalysisFactory {

    @ObjectFactory
    public AnalysisEntity createEntity(Analysis dto) {
        if (dto != null && dto.id != null) {
            AnalysisEntity entity = AnalysisEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Analysis with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new AnalysisEntity();
    }
    
}
