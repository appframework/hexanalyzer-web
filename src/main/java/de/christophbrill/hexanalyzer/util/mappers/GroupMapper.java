package de.christophbrill.hexanalyzer.util.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import de.christophbrill.hexanalyzer.ui.dto.Group;

@Mapper(uses = GroupFactory.class)
public interface GroupMapper extends ResourceMapper<Group, GroupEntity> {

    GroupMapper INSTANCE = Mappers.getMapper(GroupMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "binaries", ignore = true)
    GroupEntity mapDtoToEntity(Group group);

}
