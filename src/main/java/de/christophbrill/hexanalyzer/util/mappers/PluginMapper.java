package de.christophbrill.hexanalyzer.util.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.ui.dto.Plugin;
import de.christophbrill.hexanalyzer.util.compiler.Compiler;

@Mapper(uses = PluginFactory.class)
public interface PluginMapper extends ResourceMapper<Plugin, PluginEntity> {

    PluginMapper INSTANCE = Mappers.getMapper(PluginMapper.class);

    @Override
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "compiled", ignore = true)
    PluginEntity mapDtoToEntity(Plugin dto);

    @Override
    @Mapping(target = "compiled", ignore = true)
    Plugin mapEntityToDto(PluginEntity entity);

    @AfterMapping
    default void customDtoToEntity(Plugin dto, @MappingTarget PluginEntity entity) {
        entity.compiled = Compiler.compile(dto.qualifiedName, dto.source);
    }

    @AfterMapping
    default void customEntityToDto(PluginEntity entity, @MappingTarget Plugin dto) {
        dto.compiled = entity.compiled != null && entity.compiled.length > 0;
    }

}
