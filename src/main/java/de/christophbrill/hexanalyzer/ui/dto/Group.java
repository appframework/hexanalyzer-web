package de.christophbrill.hexanalyzer.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Group extends AbstractDto {

    public String name;
    public String description;

}
