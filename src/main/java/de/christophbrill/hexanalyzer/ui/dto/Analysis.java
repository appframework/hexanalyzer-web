package de.christophbrill.hexanalyzer.ui.dto;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Analysis extends AbstractDto {

    public Long binaryId;
    public String name;
    public String start;
    public String end;
    public String description;
    public String color;
    public String foregroundColor;

}
