package de.christophbrill.hexanalyzer.ui.dto;

import java.util.List;

public class DataSearchResult {

    public String data;
    public List<Binary> binaries;

}
