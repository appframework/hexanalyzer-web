package de.christophbrill.hexanalyzer.ui.dto;

import java.util.List;

import de.christophbrill.appframework.ui.dto.AbstractDto;

public class Binary extends AbstractDto {

    public enum AnalysisStatus {
        NOT,
        AUTO,
        PARTIAL,
        COMPLETE
    }

    public String name;
    public String data;
    public List<Analysis> analyses;
    public List<Long> groupIds;
    public AnalysisStatus status;
    public Long parentId;

}
