package de.christophbrill.hexanalyzer.ui.rest;

import java.util.List;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.persistence.model.Permission;
import de.christophbrill.hexanalyzer.persistence.selector.AnalysisSelector;
import de.christophbrill.hexanalyzer.persistence.selector.BinarySelector;
import de.christophbrill.hexanalyzer.ui.dto.Analysis;
import de.christophbrill.hexanalyzer.util.mappers.AnalysisMapper;

@Path("/analysis")
public class AnalysisService extends AbstractResourceService<Analysis, AnalysisEntity> {

    private static final Logger LOG = LoggerFactory.getLogger(AnalysisService.class);

    @Override
    protected Class<AnalysisEntity> getEntityClass() {
        return AnalysisEntity.class;
    }

    @Override
    protected AnalysisSelector getSelector() {
        return new AnalysisSelector(em);
    }

    @Override
    protected AnalysisMapper getMapper() {
        return AnalysisMapper.INSTANCE;
    }

    @PUT
    @Path("publish")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public int publish(@QueryParam("start") int start,
                        @QueryParam("end") int end,
                        @QueryParam("name") @DefaultValue("") String name,
                        @QueryParam("groupIds") List<Integer> groupIds,
                        Analysis analysis_) {

        // Skip bogus requests
        if (end < start) {
            return 0;
        }

        List<BinaryEntity> binaries = new BinarySelector(em)
                .withNameLike(name)
                .withGroupIds(groupIds)
                .findAll();

        int result = 0;
        outer: for (BinaryEntity binary : binaries) {
            for (AnalysisEntity analysis : binary.analyses) {
                if (analysis.start == start && analysis.end == end) {
                    LOG.debug("Binary {} already has a matching analysis for {} to {}", binary.id, start, end);
                    continue outer;
                }
            }
            analysis_.binaryId = binary.id;
            analysis_.id = null;
            super.create(analysis_);
            result++;
        }

        return result;

    }

    @Override
    @Transactional
    public Analysis create(Analysis analysis) {
        analysis = super.create(analysis);

        // Cache our start and end-position in the owning binary
        int start = Integer.parseInt(analysis.start, 16);
        int end = Integer.parseInt(analysis.end, 16);

        LOG.debug("Created new analysis {} in {} from {} to {}", analysis.id, analysis.binaryId, start, end);

        // Load the owner of this analysis
        BinaryEntity owner = new BinarySelector(em)
            .withId(analysis.binaryId)
            .find();

        // Push changes up the hierarchy
        {
            if (owner.parent != null) {
                AnalysisEntity parent = owner.parent;

                int existingStart = parent.start;

                analysis.id = null;
                analysis.binaryId = parent.binary.id;
                analysis.start = Integer.toString(start + existingStart, 16);
                analysis.end = Integer.toString(end + existingStart, 16);
                analysis = super.create(analysis);

                LOG.debug("Created parent analysis {} in {} from {} to {}", analysis.id, parent.binary.id, start + existingStart, end + existingStart);
            }
        }

        // Push changes down the hierarchy
        {

            for (AnalysisEntity existingChild : owner.analyses) {

                // Ignore the just created entity
                if (existingChild.id.equals(analysis.id)) {
                    continue;
                }

                // If there are binaries derived from this analysis
                if (CollectionUtils.isNotEmpty(existingChild.children)) {

                    LOG.debug("Found existing analysis {} with attached binaries", existingChild.id);

                    int existingStart = existingChild.start;

                    for (BinaryEntity other : existingChild.children) {
                        analysis.id = null;
                        analysis.binaryId = other.id;
                        analysis.start = Integer.toString(start - existingStart, 16);
                        analysis.end = Integer.toString(end - existingStart, 16);
                        analysis = super.create(analysis);

                        LOG.debug("Created child analysis {} in {} from {} to {}", analysis.id, other.id, start - existingStart, end - existingStart);
                    }
                }
            }
        }

        return analysis;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_ANALYSIS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

}
