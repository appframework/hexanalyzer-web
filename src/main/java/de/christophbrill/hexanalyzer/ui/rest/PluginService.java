/*
 * Copyright 2016  Christoph Brill <opensource@christophbrill.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.hexanalyzer.ui.rest;

import jakarta.ws.rs.Path;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.hexanalyzer.persistence.model.Permission;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.persistence.selector.PluginSelector;
import de.christophbrill.hexanalyzer.ui.dto.Plugin;
import de.christophbrill.hexanalyzer.util.mappers.PluginMapper;

@Path("/plugin")
public class PluginService extends AbstractResourceService<Plugin, PluginEntity> {

    @Override
    protected Class<PluginEntity> getEntityClass() {
        return PluginEntity.class;
    }

    @Override
    protected PluginSelector getSelector() {
        return new PluginSelector(em);
    }

    @Override
    protected PluginMapper getMapper() {
        return PluginMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_PLUGINS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_PLUGINS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_PLUGINS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_PLUGINS.name();
    }

}