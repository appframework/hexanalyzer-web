package de.christophbrill.hexanalyzer.ui.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import de.christophbrill.hexanalyzer.persistence.model.Permission;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.persistence.selector.BinarySelector;
import de.christophbrill.hexanalyzer.persistence.selector.PluginSelector;
import de.christophbrill.hexanalyzer.ui.dto.Binary;
import de.christophbrill.hexanalyzer.ui.dto.Binary.AnalysisStatus;
import de.christophbrill.hexanalyzer.ui.dto.DataSearchResult;
import de.christophbrill.hexanalyzer.ui.dto.Plugin;
import de.christophbrill.hexanalyzer.util.compiler.Compiler;
import de.christophbrill.hexanalyzer.util.mappers.AnalysisMapper;
import de.christophbrill.hexanalyzer.util.mappers.BinaryMapper;
import de.christophbrill.hexanalyzer.util.plugins.BinaryAnalyzer;

@Path("/binary")
public class BinaryService extends AbstractResourceService<Binary, BinaryEntity> {

    private static final Logger LOG = LoggerFactory.getLogger(BinaryService.class);

    private static final Comparator<DataSearchResult> COMPARATOR = Comparator.comparing((x) -> x.data);

    @Override
    protected Class<BinaryEntity> getEntityClass() {
        return BinaryEntity.class;
    }

    @Override
    protected BinarySelector getSelector() {
        return new BinarySelector(em);
    }

    @Override
    protected BinaryMapper getMapper() {
        return BinaryMapper.INSTANCE;
    }

    @Override
    protected BinaryEntity mapCreate(Binary t) {
        BinaryEntity map = super.mapCreate(t);
        if (map.analyses == null) {
            map.analyses = new ArrayList<>();
        }

        for (AnalysisEntity entity : map.analyses) {
            entity.binary = map;
        }

        map.status = AnalysisStatus.NOT;
        applyPluginExtensionPoint(map, Plugin.ExtensionPoint.CREATE_AUTO_ANALYZE);
        if (!map.analyses.isEmpty()) {
            map.status = AnalysisStatus.AUTO;
        }
        return map;
    }
    
    @Override
    protected BinaryEntity mapUpdate(Binary t) {
        var entity = super.mapUpdate(t);
        // No analyses, perform auto analysis
        if (entity.analyses == null || entity.analyses.isEmpty()) {
            entity.status = AnalysisStatus.NOT;
            applyPluginExtensionPoint(entity, Plugin.ExtensionPoint.CREATE_AUTO_ANALYZE);
            if (entity.analyses != null && !entity.analyses.isEmpty()) {
                entity.status = AnalysisStatus.AUTO;
            }
        }
        return entity;
    }

    private void applyPluginExtensionPoint(BinaryEntity map, Plugin.ExtensionPoint extensionPoint) {
        List<PluginEntity> plugins = new PluginSelector(em)
            .withExtensionPoint(extensionPoint)
            .findAll();

        for (PluginEntity plugin : plugins) {
            String qualifiedName = plugin.qualifiedName;
            assert qualifiedName != null;
            try {
                byte[] compiled = plugin.compiled;
                if (compiled != null) {
                    @SuppressWarnings("unchecked")
                    Class<? extends BinaryAnalyzer> klass = (Class<? extends BinaryAnalyzer>) Compiler.load(qualifiedName, compiled);
                    BinaryAnalyzer instance = klass.newInstance();
                    instance.analyze(map);
                } else {
                    LOG.warn("Could not execute plugin {}, it was not compiled", qualifiedName);
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                LOG.info("Failed to execute plugin {}", qualifiedName, e);
            }
        }
    }

    @GET
    @Path("extract")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DataSearchResult> extract(@QueryParam("start") int start, @QueryParam("end") int end,
            @QueryParam("name") @DefaultValue("") String name, @QueryParam("groupIds") List<Integer> groupIds) {

        // Skip bogus requests
        if (end < start) {
            return Collections.emptyList();
        }

        List<BinaryEntity> binaries = new BinarySelector(em)
                .withNameLike(name)
                .withGroupIds(groupIds)
                .withSortColumn("name", true)
                .findAll();

        Map<String, DataSearchResult> lookup = new HashMap<>(binaries.size());
        for (BinaryEntity binary : binaries) {
            // Skip if not enough data available
            String data;
            if (binary.data.length() < (end+1)*2) {
                data = "to short";
            } else {
                data = binary.data.substring(start*2, (end+1)*2);
            }
            DataSearchResult dataSearchResult = lookup.get(data);
            if (dataSearchResult == null) {
                dataSearchResult = new DataSearchResult();
                dataSearchResult.data = padSplit(data);
                lookup.put(data, dataSearchResult);
                dataSearchResult.binaries = new ArrayList<>(1);
            }
            Binary b = new Binary();
            b.name = binary.name;
            b.id = binary.id;
            for (AnalysisEntity analysis : binary.analyses) {
                if (analysis.start == start && analysis.end == end) {
                    b.analyses = Collections.singletonList(AnalysisMapper.INSTANCE.mapEntityToDto(analysis));
                }
            }
            dataSearchResult.binaries.add(b);
        }

        List<DataSearchResult> result = new ArrayList<>(lookup.values());
        result.sort(COMPARATOR);
        return result;
    }

    private String padSplit(String data) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length(); i += 16) {
            builder.append(data, i, Math.min(i+16, data.length()));
            if (i+16 < data.length()) {
                builder.append(' ');
            }
        }
        return builder.toString();
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_BINARIES.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_BINARIES.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_BINARIES.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_BINARIES.name();
    }

}
