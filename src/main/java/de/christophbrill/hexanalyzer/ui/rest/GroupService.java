package de.christophbrill.hexanalyzer.ui.rest;

import jakarta.ws.rs.Path;

import de.christophbrill.appframework.ui.rest.AbstractResourceService;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import de.christophbrill.hexanalyzer.persistence.model.Permission;
import de.christophbrill.hexanalyzer.persistence.selector.GroupSelector;
import de.christophbrill.hexanalyzer.ui.dto.Group;
import de.christophbrill.hexanalyzer.util.mappers.GroupMapper;

@Path("/group")
public class GroupService extends AbstractResourceService<Group, GroupEntity> {

    @Override
    protected Class<GroupEntity> getEntityClass() {
        return GroupEntity.class;
    }

    @Override
    protected GroupSelector getSelector() {
        return new GroupSelector(em);
    }

    @Override
    protected GroupMapper getMapper() {
        return GroupMapper.INSTANCE;
    }

    @Override
    protected String getCreatePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

    @Override
    protected String getReadPermission() {
        return Permission.SHOW_ANALYSIS.name();
    }

    @Override
    protected String getUpdatePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

    @Override
    protected String getDeletePermission() {
        return Permission.ADMIN_ANALYSIS.name();
    }

}
