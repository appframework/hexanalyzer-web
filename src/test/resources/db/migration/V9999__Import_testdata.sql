TRUNCATE TABLE
    analysis,
    binary_,
    binary_data,
    group_,
    plugin,
    role,
    role_permissions,
    user_,
    user_role;

INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (1,'Tom Tester','tom','ab4d8d2a5f480a137067da17100271cd176607a1','tom@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (2,'XXX','xxx','b60d121b438a380c343d5ec3c2037564b82ffef3','xxx@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO user_(id,name,login,password,email,created,modified,creator_id,modificator_id) VALUES (3,'YYY','yyy','186154712b2d5f6791d85b9a0987b98fa231779c','yyy@localhost',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE user__SEQ RESTART WITH 4;

INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (1,'Administrators',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (2,'Users',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO role(id,name,created,modified,creator_id,modificator_id) VALUES (3,'No Permissions',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE role_SEQ RESTART WITH 4;

INSERT INTO role_permissions(role_id,permission) VALUES (1,'ADMIN_USERS'), (1,'ADMIN_ROLES'), (1,'SHOW_USERS'), (1,'SHOW_ROLES'), (1,'ADMIN_ANALYSIS'), (1,'SHOW_ANALYSIS'), (1,'ADMIN_BINARIES'), (1,'SHOW_BINARIES'), (1,'ADMIN_PLUGINS'), (1,'SHOW_PLUGINS');
INSERT INTO role_permissions(role_id,permission) VALUES (2,'SHOW_TASKS');

INSERT INTO user_role(user_id,role_id) VALUES (1,1);
INSERT INTO user_role(user_id,role_id) VALUES (2,2);
INSERT INTO user_role(user_id,role_id) VALUES (3,3);

INSERT INTO group_ (id,name,created,modified,creator_id,modificator_id) VALUES (1,'Some group',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO group_ (id,name,created,modified,creator_id,modificator_id) VALUES (2,'Some other group',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE group__SEQ RESTART WITH 3;

INSERT INTO binary_ (id,name,data,created,modified,creator_id,modificator_id) VALUES (1,'Test binary #1','0123456789ABCDEF',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
INSERT INTO binary_ (id,name,data,created,modified,creator_id,modificator_id) VALUES (2,'Test binary #2','ABCDEF0123456789',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,NULL);
ALTER SEQUENCE binary__SEQ RESTART WITH 3;