package de.christophbrill.hexanalyzer.ui;

import java.util.UUID;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.hexanalyzer.ui.dto.Group;
import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class GroupCRUDTest extends AbstractCRUDTest<Group> {

	@Override
	protected Group createFixture() {
		Group fixture = new Group();
		fixture.name = UUID.randomUUID().toString();
		return fixture;
	}

	@Override
	protected String getPath() {
		return "group";
	}

	@Override
	protected Class<Group> getFixtureClass() {
		return Group.class;
	}

	@Override
	protected void modifyFixture(Group fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Group fixture, Group created) {
		assertThat(created.name).isEqualTo(fixture.name);
	}

}
