package de.christophbrill.hexanalyzer.ui;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.hexanalyzer.ui.dto.Plugin;
import de.christophbrill.hexanalyzer.ui.dto.Plugin.ExtensionPoint;
import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class PluginCRUDTest extends AbstractCRUDTest<Plugin> {

	@Override
	protected Plugin createFixture() {
		Plugin fixture = new Plugin();
		fixture.qualifiedName = "MyBinaryAnalyzer";
		fixture.source = "import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;\n"
				+ "import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;\n"
				+ "import de.christophbrill.hexanalyzer.util.plugins.BinaryAnalyzer;\n"
				+ "\n"
				+ "public class MyBinaryAnalyzer implements BinaryAnalyzer {\n"
				+ "	public void analyze(BinaryEntity binary) {\n"
				+ "		System.err.println(binary);\n"
				+ "	}\n"
				+ "}";
		fixture.extensionPoint = ExtensionPoint.CREATE_AUTO_ANALYZE;
		return fixture;
	}

	@Override
	protected String getPath() {
		return "plugin";
	}

	@Override
	protected Class<Plugin> getFixtureClass() {
		return Plugin.class;
	}

	@Override
	protected void modifyFixture(Plugin fixture) {
		fixture.source = "import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;\n"
				+ "import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;\n"
				+ "import de.christophbrill.hexanalyzer.util.plugins.BinaryAnalyzer;\n"
				+ "\n"
				+ "public class MyBinaryAnalyzer implements BinaryAnalyzer {\n"
				+ "	public void analyze(BinaryEntity binary) {\n"
				+ "		System.out.println(binary);\n"
				+ "	}\n"
				+ "}";
	}

	@Override
	protected void compareDtos(Plugin fixture, Plugin created) {
		assertThat(created.source).isEqualTo(fixture.source);
	}

}
