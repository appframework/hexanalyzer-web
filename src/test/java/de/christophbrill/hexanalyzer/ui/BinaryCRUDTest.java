package de.christophbrill.hexanalyzer.ui;

import java.util.UUID;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.hexanalyzer.ui.dto.Binary;
import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class BinaryCRUDTest extends AbstractCRUDTest<Binary> {

	@Override
	protected Binary createFixture() {
		Binary fixture = new Binary();
		fixture.name = UUID.randomUUID().toString();
		fixture.data = UUID.randomUUID().toString();
		return fixture;
	}

	@Override
	protected String getPath() {
		return "binary";
	}

	@Override
	protected Class<Binary> getFixtureClass() {
		return Binary.class;
	}

	@Override
	protected void modifyFixture(Binary fixture) {
		fixture.name = UUID.randomUUID().toString();
	}

	@Override
	protected void compareDtos(Binary fixture, Binary created) {
		assertThat(created.name).isEqualTo(fixture.name);
	}

}
