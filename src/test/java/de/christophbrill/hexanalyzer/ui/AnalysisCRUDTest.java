package de.christophbrill.hexanalyzer.ui;

import java.util.UUID;

import de.christophbrill.appframeworktesting.ui.AbstractCRUDTest;
import de.christophbrill.hexanalyzer.ui.dto.Analysis;
import de.christophbrill.hexanalyzer.ui.dto.Binary;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class AnalysisCRUDTest extends AbstractCRUDTest<Analysis> {

    @Override
    protected Analysis createFixture() {
        Binary binary = createBinary();

        Analysis fixture = new Analysis();
        fixture.name = UUID.randomUUID().toString();
        fixture.binaryId = binary.id;
        fixture.start = "00";
        fixture.end = "0A";
        return fixture;
    }

    private Binary createBinary() {
        BinaryCRUDTest binaryTest = new BinaryCRUDTest();
        Binary binary = binaryTest.createFixture();
        binary = given()
            .auth().preemptive().basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(binary)
            .post("/rest/binary")
            .as(Binary.class);
        return binary;
    }

    @Override
    protected String getPath() {
        return "analysis";
    }

    @Override
    protected Class<Analysis> getFixtureClass() {
        return Analysis.class;
    }

    @Override
    protected void modifyFixture(Analysis fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Analysis fixture, Analysis created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }

}
