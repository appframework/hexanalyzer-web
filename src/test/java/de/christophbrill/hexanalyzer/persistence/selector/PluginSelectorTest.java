package de.christophbrill.hexanalyzer.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class PluginSelectorTest extends AbstractResourceSelectorTest<PluginEntity> {

	@Override
	protected PluginSelector getSelector() {
		return new PluginSelector(em);
	}

}
