package de.christophbrill.hexanalyzer.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class AnalysisSelectorTest extends AbstractResourceSelectorTest<AnalysisEntity> {

	@Override
	protected AnalysisSelector getSelector() {
		return new AnalysisSelector(em);
	}

}
