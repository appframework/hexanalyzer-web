package de.christophbrill.hexanalyzer.persistence.selector;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class GroupSelectorTest extends AbstractResourceSelectorTest<GroupEntity> {

	@Override
	protected GroupSelector getSelector() {
		return new GroupSelector(em);
	}

}
