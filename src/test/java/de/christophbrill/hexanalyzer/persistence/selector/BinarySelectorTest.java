package de.christophbrill.hexanalyzer.persistence.selector;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import de.christophbrill.appframeworktesting.persistence.selector.AbstractResourceSelectorTest;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class BinarySelectorTest extends AbstractResourceSelectorTest<BinaryEntity> {

	@Override
	protected BinarySelector getSelector() {
		return new BinarySelector(em);
	}

	@Test
	public void testNameLike() {
		long count = getSelector().count();
		
		assertThat(getSelector().withNameLike(null).count()).isEqualTo(count);

		assertThat(getSelector().withNameLike("").count()).isEqualTo(count);

		assertThat(getSelector().withNameLike("#").count()).isEqualTo(2L);
		assertThat(getSelector().withNameLike("#2").count()).isEqualTo(1L);
		assertThat(getSelector().withNameLike("#1").count()).isEqualTo(1L);

		assertThat(getSelector().withNameLike(UUID.randomUUID().toString()).count()).isEqualTo(0L);
	}
}
