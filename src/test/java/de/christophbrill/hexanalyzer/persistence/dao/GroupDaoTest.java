package de.christophbrill.hexanalyzer.persistence.dao;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.hexanalyzer.persistence.model.GroupEntity;
import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class GroupDaoTest extends AbstractDaoCRUDTest<GroupEntity> {

    @Override
    protected GroupEntity createFixture() {
        GroupEntity user = new GroupEntity();
        user.name = UUID.randomUUID().toString();
        return user;
    }

    @Override
    protected void modifyFixture(GroupEntity user) {
        user.name = UUID.randomUUID().toString();
    }

    @Override
    protected Long count() {
        return GroupEntity.count();
    }

    @Override
    protected GroupEntity findById(Long id) {
        return GroupEntity.findById(id);
    }

    @Test
    public void testFindByIds() {
        List<GroupEntity> all = GroupEntity.findAll().list();

        assertThat(all).isEqualTo(GroupEntity.findByIds(null));
        assertThat(all).isEqualTo(GroupEntity.findByIds(Collections.emptyList()));

        Assumptions.assumeTrue(all.size() >= 2);

        GroupEntity group = all.get(0);
        assertThat(group).isEqualTo(GroupEntity.findByIds(Collections.singletonList(group.id)).iterator().next());
    }
}
