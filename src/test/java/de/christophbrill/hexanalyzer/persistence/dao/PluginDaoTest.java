package de.christophbrill.hexanalyzer.persistence.dao;

import java.util.UUID;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.hexanalyzer.persistence.model.PluginEntity;
import de.christophbrill.hexanalyzer.ui.dto.Plugin.ExtensionPoint;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class PluginDaoTest extends AbstractDaoCRUDTest<PluginEntity> {

    @Override
    protected PluginEntity createFixture() {
        PluginEntity plugin = new PluginEntity();
        plugin.qualifiedName = UUID.randomUUID().toString();
        plugin.source = "source";
        plugin.extensionPoint = ExtensionPoint.CREATE_AUTO_ANALYZE;
        return plugin;
    }

    @Override
    protected void modifyFixture(PluginEntity user) {
        user.qualifiedName = UUID.randomUUID().toString();
    }

    @Override
    protected Long count() {
        return PluginEntity.count();
    }

    @Override
    protected PluginEntity findById(Long id) {
        return PluginEntity.findById(id);
    }

}
