package de.christophbrill.hexanalyzer.persistence.dao;

import java.util.UUID;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class BinaryDaoTest extends AbstractDaoCRUDTest<BinaryEntity> {

    @Override
    protected BinaryEntity createFixture() {
        BinaryEntity binary = new BinaryEntity();
        binary.name = UUID.randomUUID().toString();
        binary.data = UUID.randomUUID().toString().replace("-", "");
        return binary;
    }

    @Override
    protected void modifyFixture(BinaryEntity user) {
        user.name = UUID.randomUUID().toString();
    }

    @Override
    protected Long count() {
        return BinaryEntity.count();
    }

    @Override
    protected BinaryEntity findById(Long id) {
        return BinaryEntity.findById(id);
    }

}
