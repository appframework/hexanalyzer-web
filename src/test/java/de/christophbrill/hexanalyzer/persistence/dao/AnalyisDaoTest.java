package de.christophbrill.hexanalyzer.persistence.dao;

import java.util.UUID;

import de.christophbrill.appframeworktesting.persistence.dao.AbstractDaoCRUDTest;
import de.christophbrill.hexanalyzer.persistence.model.AnalysisEntity;
import de.christophbrill.hexanalyzer.persistence.model.BinaryEntity;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class AnalyisDaoTest extends AbstractDaoCRUDTest<AnalysisEntity> {

    @Override
    protected AnalysisEntity createFixture() {
        BinaryEntity binary = new BinaryDaoTest().createFixture();
        BinaryEntity.persist(binary);

        AnalysisEntity analysis = new AnalysisEntity();
        analysis.name = UUID.randomUUID().toString();
        analysis.binary = binary;
        analysis.start = 0;
        analysis.end = 10;
        return analysis;
    }

    @Override
    protected void modifyFixture(AnalysisEntity user) {
        user.name = UUID.randomUUID().toString();
    }

    @Override
    protected Long count() {
        return AnalysisEntity.count();
    }

    @Override
    protected AnalysisEntity findById(Long id) {
        return AnalysisEntity.findById(id);
    }

}
